import React, { useEffect, useRef, useState } from "react";
import { ProductCard } from "../../components/Elements/ProductCard";
import { Filter } from "./components/Filter";
import useFetch from "../../hooks/useFetch";

export const ProductPage = () => {
    const [show, setShow] = useState(false);
    const filterRef = useRef();

    useEffect(() => {
        const handleClickOutside = (evt) => {
            if(filterRef.current && !filterRef.current.contains(evt.target)) {
                setShow(false);
            }
        };

        // Attach Evt listener
        document.addEventListener("mousedown", handleClickOutside);

        // cleanup
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [filterRef])
    const { data: products, error, isLoading } = useFetch("http://localhost:8000/products");


    return (
        <main className="my-5">
            <div className="flex justify-between">
                <h2 className="text-2xl font-semibold dark:text-slate-100 mb-5 section-title">
                    All Plants (20)
                </h2>
                <span>
                    <button
                        class="text-white bg-primary-700 hover:bg-primary-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none dark:focus:ring-blue-800"
                        type="button"
                        data-drawer-target="drawer-navigation"
                        data-drawer-show="drawer-navigation"
                        data-drawer-body-scrolling="false"
                        aria-controls="drawer-disable-body-scrolling"
                        onClick={() => setShow(!show)}
                    >
                        Filter
                    </button>
                </span>
            </div>
            <Filter show={show} setShow={setShow} refProp={filterRef}/>
            <div className="flex flex-wrap justify-left lg:flex-row">
                {
                    products && products.map(product => <ProductCard product={product} />)
                }
            </div>
        </main>
    );
};
