import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { Search } from "./Search";
import Logo from '../../../assets/Logo.png';
import DarkLogo from '../../../assets/DarkLogo.png'
import { DropDownMenu } from "./DropDownMenu";


export const Header = () => {
    const [darkMode, setDarkMode] = useState(
        JSON.parse(localStorage.getItem("darkMode")) || false
    );

    const [dropdown, setDropdown] = useState(false);
    const dropdownRef = useRef(null);
    const [searchSection, setSearchSection] = useState(false);

    useEffect(() => {
        localStorage.setItem("darkMode", JSON.stringify(darkMode));

        if(darkMode) {
            document.documentElement.classList.add("dark");
        } else {
            document.documentElement.classList.remove("dark");
        }
    }, [darkMode]);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if(dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                //  Clicked outside the dropdown, close it
                setDropdown(false);
            }
        };

        //  Attach the event Listener
        document.addEventListener("mousedown", handleClickOutside);

        //  Clean up the event Listener on component unmount
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        }
    }, [dropdownRef]);

    return (
        <header>
            <nav class="bg-primary-700 dark:bg-slate-800 border-gray-200 dark:bg-gray-900">
                <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
                    <Link to="/" class="flex items-center space-x-3 rtl:space-x-reverse">
                        <img src={darkMode ? DarkLogo : Logo} class="h-14" alt="Purity Plants Logo" />
                        <span class="sr-only">Purity Plants</span>
                    </Link>
                    <div class="flex items-center relative">
                        <span 
                            className={`hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-${darkMode ? "sun-fill" : "moon-fill"}`}
                            onClick={() => setDarkMode(!darkMode)}
                        >
                        </span>
                        <span
                            onClick={() => setSearchSection(!searchSection)} className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-search"
                        >
                        </span>
                        <span className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-cart"></span>
                        <span
                            onClick={() => {setDropdown(!dropdown)}}
                            className="hover:cursor-pointer text-xl text-slate-800 dark:text-slate-50 mr-5 bi bi-person-circle"
                        ></span>
                        {dropdown && <DropDownMenu setDropdown={setDropdown} refProp={dropdownRef} />}
                    </div>
                </div>
            </nav>
            {searchSection && <Search setSearchSection={setSearchSection} />}
        </header>
    );
};
