import React from "react";
import { Route, Routes } from "react-router-dom";
import { HomePage, ProductDetailPage, ProductPage } from "../pages";


export const AppRoutes = () => {
    return (
        <>
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/products' element={<ProductPage />} />
                <Route path="/products/:id" element={<ProductDetailPage />} />
            </Routes>
        </>
    )
};
