import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { SliderCard } from "./SliderCard";

const testimonials = [
    {
        id: 1,
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, culpa! Nesciunt nihil deserunt consequuntur",
        author: "Jhon Doe",
        company: "ABC"
    },
    {
        id: 2,
        text: "veniam id porro corrupti consequuntur nulla magnam odio blanditiis!",
        author: "Wiliam Doe",
        company: "PQR"
    },
    {
        id: 3,
        text: "minus dolorem provident harum vel voluptas laudantium, ipsam alias.",
        author: "Sahil Israni",
        company: "XYZ"
    },
]

export const Testimonial = () => {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    return (
        <div className="mb-12">
            <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
                Pure Reviews
            </h2>
            <Slider { ...settings }>
                {
                    testimonials.map(testimonial => <SliderCard testimonial= {testimonial } />)
                }
            </Slider>
        </div>
    )
};
