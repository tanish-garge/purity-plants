import React, { useEffect } from "react";
import { Rating } from "../../components/Elements/Rating";
import { useParams } from "react-router-dom";
import useFetch from "../../hooks/useFetch";

export const ProductDetailPage = () => {
    const params = useParams();
    const {data: product, setUrl } = useFetch();

    useEffect(() => {
        const productId = params.id;
        const url = `http://localhost:8000/products/${productId}`;
        setUrl(url);
    }, [params.id, setUrl]);

    return (
        <main>
            { product && (
                    <section>
                        <h1
                            class="mt-10 mb-5 text-4xl text-center font-bold text-slate-900 dark:text-slate-200"
                        >
                            {product.name}
                        </h1>
                        <p class="mb-5 text-lg text-center text-slate-900 dark:text-slate-200">
                            {product.overview}
                        </p>
                        <div class="flex flex-wrap justify-around">
                            <div class="max-w-xl my-3">
                                <img
                                    class="rounded"
                                    src={product.poster}
                                    alt="Philodendron Broken Heart Plant"
                                />
                            </div>
                            <div class="max-w-xl my-3">
                                <p class="text-3xl font-bold text-gray-900 dark:text-slate-200">
                                    <span class="mr-1">$</span><span class="">{product.price}</span>
                                </p>
                                <p class="my-3">
                                    <span>
                                        <Rating rating={product.rating}/>
                                    </span>
                                </p>
                                <p class="my-4 select-none">
                                    <span
                                        class="font-semibold text-emerald-600 border bg-slate-100 rounded-lg px-3 py-1 mr-2"
                                    >
                                        {
                                            product.in_stock ? "INSTOCK" : "Out Of Stock"
                                        }
                                    </span>
                                    <span
                                        class="font-semibold text-blue-500 border bg-slate-100 rounded-lg px-3 py-1 mr-2"
                                    >
                                        {
                                            product.size > 1 ? `${product.size} Inches` : `${product.size} Inch`
                                        }
                                    </span>
                                </p>
                                <p class="my-3">
                                    <button
                                        type="submit"
                                        class="hover:cursor-pointer text-white bg-primary-700 hover:bg-primary-900 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                                    >
                                        Add To Cart
                                    </button>
                                </p>
                                <p class="text-md text-gray-900 dark:text-slate-200">
                                    {product.long_description}
                                </p>
                            </div>
                        </div>
                    </section>
                )
            }
        </main>
    )
};
