import React from "react";

export const Filter = ({ show, setShow , refProp }) => {
    const allRequiredClasses =
        "fixed top-0 left-0 z-40 h-screen p-4 overflow-y-auto transition-transform bg-white w-64 dark:bg-gray-800";

    const offCanvasClass = show
        ? `${allRequiredClasses} transform-none`
        : `${allRequiredClasses} -translate-x-full`;

    return (
        <div
            id="drawer-disable-body-scrolling"
            class={offCanvasClass}
            tabindex="-1"
            aria-labelledby="drawer-disable-body-scrolling-label"
            ref={refProp}
        >
            <h5
                id="drawer-disable-body-scrolling-label"
                class="text-base font-semibold text-gray-500 uppercase dark:text-gray-400"
            >
                Filters
            </h5>
            <button
                type="button"
                data-drawer-hide="drawer-disable-body-scrolling"
                aria-controls="drawer-disable-body-scrolling"
                class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 absolute top-2.5 end-2.5 inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white"
                onClick={() => setShow(false)}
            >
                <svg
                    class="w-3 h-3"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 14"
                >
                    <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                    />
                </svg>
                <span class="sr-only">Close menu</span>
            </button>
            <div className="py-4 overflow-y-auto dark:text-slate-100 text-slate-700">
                <div>
                    <h2 className="font-bold dark:text-slate-100">Sort By</h2>
                    <div className="form-control">
                        <label htmlFor="lowToHigh">
                            <span className="ml-2"></span>
                            <input type="radio" id="lowToHigh" />
                            <span className="ml-2"></span>
                            Price - Low to High
                        </label>
                    </div>
                    <div className="form-control">
                        <label htmlFor="highToLow">
                            <span className="ml-2"></span>
                            <input type="radio" id="highToLow" />
                            <span className="ml-2"></span>
                            Price - High to Low
                        </label>
                    </div>
                </div>
                <div className="mt-5">
                    <h2 className="font-bold dark:text-slate-100">Rating</h2>
                    <div className="form-control">
                        <label htmlFor="fourNAbove">
                            <span className="ml-2"></span>
                            <input type="radio" id="fourNAbove" />
                            <span className="ml-2"></span>4 Stars &amp; Above
                        </label>
                    </div>
                    <div className="form-control">
                        <label htmlFor="threeNAbove">
                            <span className="ml-2"></span>
                            <input type="radio" id="threeNAbove" />
                            <span className="ml-2"></span>3 Stars &amp; Above
                        </label>
                    </div>
                    <div className="form-control">
                        <label htmlFor="twoNAbove">
                            <span className="ml-2"></span>
                            <input type="radio" id="twoNAbove" />
                            <span className="ml-2"></span>2 Stars &amp; Above
                        </label>
                    </div>
                    <div className="form-control">
                        <label htmlFor="oneNAbove">
                            <span className="ml-2"></span>
                            <input type="radio" id="oneNAbove" />
                            <span className="ml-2"></span>1 Stars &amp; Above
                        </label>
                    </div>
                </div>
                <div className="mt-5">
                    <h2 className="font-bold dark:text-slate-100">
                        Other Filters
                    </h2>
                    <div className="form-control">
                        <label htmlFor="bestSeller">
                            <span className="ml-2"></span>
                            <input
                                type="checkbox"
                                id="bestSeller"
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded dark:bg-gray-700 dark:border-gray-600"
                            />
                            <span className="ml-2"></span>
                            Bestseller Only
                        </label>
                    </div>
                    <div className="form-control">
                        <label htmlFor="inStock">
                            <span className="ml-2"></span>
                            <input
                                type="checkbox"
                                id="inStock"
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded dark:bg-gray-700 dark:border-gray-600"
                            />
                            <span className="ml-2"></span>
                            In-Stock Only
                        </label>
                    </div>
                </div>
                <div className="mt-5 text-center">
                    <button className="text-white bg-primary-700 hover:bg-primary-800 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-primary-600 dark:hover:bg-primary-700 focus:outline-none">
                        Clear Filter
                    </button>
                </div>
            </div>
        </div>
    );
};
