import React from "react";
import { Accordion } from "./Accordion";

export const Faq = () => {
    const faqs = [
        {
            id: 1,
            question: "Are Plants Good For Health?",
            answer: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae et adipisci nihil atque odit dolorem, cumque sint. Totam, id omnis? Ipsam quas vel perspiciatis assumenda rem ad, ullam voluptas doloribus."
        },
        {
            id: 2,
            question: "Can Plants Grow In Artificial Light?",
            answer: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae et adipisci nihil atque odit dolorem, cumque sint. Totam, id omnis? Ipsam quas vel perspiciatis assumenda rem ad, ullam voluptas doloribus."
        },
        {
            id: 1,
            question: "Are plants safe?",
            answer: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae et adipisci nihil atque odit dolorem, cumque sint. Totam, id omnis? Ipsam quas vel perspiciatis assumenda rem ad, ullam voluptas doloribus."
        },
    ]
    return (
        <div>
            <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
                FAQs
            </h2>
            <div id="accordion-open" data-accordion="open">
                {faqs.map(faq => <Accordion key={faq.id} question={faq.question} answer={faq.answer}/>)}
            </div>
        </div>
    )
};
