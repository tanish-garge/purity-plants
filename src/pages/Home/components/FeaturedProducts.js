import React from "react";
import { ProductCard } from "../../../components/Elements/ProductCard";
import useFetch from "../../../hooks/useFetch";

export const FeaturedProducts = () => {
    const { data: featuredProducts, error, isLoading } = useFetch("http://localhost:8000/featured-products");
    return (
        <section className="my-20">
            <h2 className="text-3xl text-center font-semibold dark:text-slate-100 mb-5 section-title">
                Featured Products
            </h2>
            <div className="flex flex-wrap justify-center lg:flex-row">
                {
                    featuredProducts && featuredProducts.map(product => <ProductCard key={product.id} product={product} />)
                }
            </div>
        </section>
    );
};
